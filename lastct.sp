#pragma semicolon 1
#include <sourcemod>
#include <cstrike>
#include <sdktools>
#include <sdkhooks>
#include <clientprefs>
#include <csgocolors>
#include <cstrike>

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))


#define PL_VERSION "1.0"

public Plugin myinfo = 
{
	name        = "lastCT",
	author		= ".#Zipcore´",
	description = "",
	version     = PL_VERSION,
	url         = "zipcore#googlemail.com"
};

float g_fJoinedCT[MAXPLAYERS + 1];

public void OnPluginStart()
{
	AddCommandListener(Hook_ChangeTeamChange, "jointeam");
	
	RegConsoleCmd("sm_lastct", Cmd_LastCT);
}

public Action Hook_ChangeTeamChange(int iClient, const char[] command, int iArgs)
{
	char arg[4];
	GetCmdArg(1, arg, sizeof(arg));
	
	int team_to = StringToInt(arg);
	int team_from = GetClientTeam(iClient);
	
	if(team_to == CS_TEAM_CT && team_from != CS_TEAM_CT)
	{
		g_fJoinedCT[iClient] = GetEngineTime();
		return Plugin_Continue;
	}
	
	return Plugin_Continue;
}

public Action Cmd_LastCT(int iClient, int iArgs)
{
	int iLast;
	float fLast;
	
	LoopIngameClients(i)
	{
		if(GetClientTeam(i) != CS_TEAM_CT)
			continue;
		
		if(fLast < g_fJoinedCT[i])
		{
			iLast = i;
			fLast = g_fJoinedCT[i];
		}
	}
	
	if(iLast != 0)
		CPrintToChat(iClient, "{blue}Last CT:  {green}%N", iLast);
	else CPrintToChat(iClient, "{blue}Last CT:  {green}unknown");
	
	return Plugin_Handled;
}